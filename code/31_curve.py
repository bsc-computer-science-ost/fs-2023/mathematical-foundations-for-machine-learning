import numpy as np
from matplotlib import pyplot as plt

t = np.linspace(-10, 10, 10)
x = t**2
y = t * (np.abs(t) - 2)
plt.plot(x, y, "r")  # red
plt.axis([-1, 7, -4, 4])  # and shrink the area that is plotted
plt.show()  # show the plot