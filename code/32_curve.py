from matplotlib import pyplot as plt
from numpy import linspace, sin

t = linspace(-10, 10, 1000)
x = sin(t)
y = t**2
plt.plot(x, y, "r")  # red
plt.axis([-10, 10, -1, 100])  # and shrink the area that is plotted
plt.show()  # show the plot
