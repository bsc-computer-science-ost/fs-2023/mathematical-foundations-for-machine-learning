import numpy as np
import numpy.linalg as LA

#### Use the np.array() command to define the list of basis vectors
#### and the target vector
a = np.array([1, 2, 3])
b = np.array([4, 5, 6])
c = np.array([0, 0, 1])

target = np.array([7, 8, 9])

#### Calculate the coordinates of target within the basis {a, b, c}
#### HINT: You need the numpy-methods np.column_stack() and LA.solve()

basis_matrix = np.column_stack([a, b, c])
coordinates = LA.solve(basis_matrix, target)

print(coordinates)

assert(np.isclose(
    LA.norm(np.dot(np.column_stack([a, b, c]), coordinates) - target), 0)
)