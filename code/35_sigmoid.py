import numpy as np
from matplotlib import pyplot

def sigmoid(x):
    return 1 / (1 + np.exp(-x))


x=np.linspace(-10,10,500)
y=sigmoid(x)
pyplot.plot(x, y)
pyplot.show()