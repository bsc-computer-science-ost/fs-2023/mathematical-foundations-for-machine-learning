from matplotlib import pyplot
import numpy as np
from numpy import sin, cos, pi

t = np.linspace(0, 6 * pi, 1000) # create 1000 numbers between 0 and 6 pi

f = [t * cos(t),
     t * sin(t) ]

t = np.linspace(0, pi, 10)       # create 10 numbers between 0 and pi
f_lin = [-pi / 2 * ( t- pi / 2),
         pi / 2 + (t - pi / 2)]

pyplot.plot(*f, 'r')             # plot the curve f in red
pyplot.plot(*f_lin, 'g')         # plot the linearisation of f in green
pyplot.axis('equal')             # use identical scales for both axis
pyplot.show()                    # display the result