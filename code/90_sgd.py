import numpy as np
from matplotlib import pyplot
from numpy import exp, sqrt
from numpy.linalg import norm

def f(x, y):
    return  y ** 3 - y + x ** 2

# create a contour plot for the function f
x, y = np.meshgrid(np.linspace(-1, 1, 30), np.linspace(-1, 1, 30))
z = f(x, y)
pyplot.contour(x, y, z, 30)

# plot stationary points in red
pyplot.scatter([0, 0], [1/sqrt(3), -1/sqrt(3)], c="red")

# TODO adjust implementation of method df(x, y) such that it returns
#      the gradient of f(x, y)
def df(x, y):
    return [2 * x, 3 * y ** 2 - 1]

# TODO adjust implementation of method grad_desc_step(x, y, df, learning_rate)
#      such that it returns the new coordinates for x and y after one step
#      of iteration of gradient descent
def grad_desc_step(x, y, df, learning_rate):
    return [x, y] - learning_rate * np.array(df(x, y))

# initialization data for gradient descent
epsilon = 0.000001     # abort when steps are getting smaller than epsilon
learning_rate = 0.6      # initial learning rate
v_start = [-0.5, -0.45]  # initial starting point
pyplot.scatter(*v_start, c="blue") # plot the starting position in blue
dist = 1

# gradient descent loop
# TODO: adjust the implementation of the gradient descent loop, such that
#       it shows the coordinates of each gradient descent step in the contour
#       plot and such that the coordinates of the minimum are contained in v_next
while dist > epsilon:
    v_next = grad_desc_step(*v_start, df, learning_rate)
    pyplot.scatter(*v_next, s=0.8, c="blue")
    dist = norm(v_next - v_start)
    v_start = v_next

# duisplay the result on screen
print ("Minimum detected at ", *v_next)
pyplot.show()
