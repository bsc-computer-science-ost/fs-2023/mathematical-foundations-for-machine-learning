from mayavi import mlab
import numpy as np
from numpy import sin, cos, pi

x = np.mgrid[-2:2:50j, 1:8:50j] # create a grid from -2 to 2 and 1 to 8

f = x[0]**2 * cos(x[1]) / (x[1] + 3) # evaluate the function on the grid

# plot the surface
mlab.mesh(*x, f) # plot the function

# ---------------- Commands to beautify the output ------------------
# add coordintate axes
mlab.axes(extent=[-2, 2, 1, 8, -2, 2], nb_labels=3,
          xlabel='x1', ylabel='x2', zlabel='f(x)')
# add a bounding box
mlab.outline(extent=[-2, 2, 1, 8, -2, 2])
mlab.show()

# plot 20 contour lines
mlab.contour3d(*x, np.zeros_like(f), f, contours=20)
mlab.axes(extent=[-2, 2, 1, 8, 0, 0], nb_labels=3, xlabel='x1', ylabel='x2')
mlab.outline(extent=[-2, 2, 1, 8, 0, 0])
mlab.view(0, 0) # set viewing perspective to "from the top"
mlab.show()
