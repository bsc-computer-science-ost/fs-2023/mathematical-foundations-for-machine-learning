import numpy as np
import matplotlib.pyplot as plt

t = np.linspace(-2, 2, 100)  # 100 values between -2 and 2
x = t ** 2  # numpy takes care for evaluating all values
y = t ** 4
plt.plot(x, y, "r")  # red
x = t ** 3
y = t ** 4
plt.plot(x, y, "b")  # blue
x = t ** 2
y = t
plt.plot(x, y, "g")  # green

t = np.linspace(0.0001, 3, 100)  # domain is positive
x = np.exp(t)  # numpy takes care for evaluating all values
y = np.log(t)
plt.plot(x, y, "y")  # yellow

plt.axis("equal")  # make axis of equal size
plt.axis([-4, 4, -4, 4])  # and shrink the area that is plotted
plt.show()  # show the plot