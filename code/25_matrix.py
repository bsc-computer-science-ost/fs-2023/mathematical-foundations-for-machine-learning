# Use Python (commands Mathematical Equation , Mathematical Equation ,
# Mathematical Equation from the numpy-package and Mathematical Equation
# from the numpy.linalg-package) to calculate the following components for
# all possible values of Mathematical Equation and Mathematical Equation by
# first bringing the formula into matrix form

import numpy as np
import numpy.linalg as LA

A = np.array([[1, 2, 3],
              [5, 7, 9],
              [0, 8, 2]])
B = np.array([[4, 7, 2],
              [1, 5, 7],
              [2, 6, 2]])
C = np.array([[5, 4, 1],
              [5, 4, 1],
              [7, 2, 1]])

print( A + np.transpose(A))
print(np.dot(A, np.dot(B, C + np.transpose(B))))
print(LA.inv(A))
